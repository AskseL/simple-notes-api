<?php

class apiController{

  public static function getUserInfo($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    $user = $db->getRow("SELECT uid, first_name, last_name, photo_100 FROM ?n WHERE uid=?i", "users", (int)$user['uid']);
    return $res->withJSON($user);
  }

  public static function getUsers($req, $res, $app){
    $user = $req->getAttribute('user');
    if($user['uid'] != 52768692) return $res->withStatus(403)->write('403 Forbidden');
    
    $db = $app->getContainer()->get('db');
    $users = $db->getAll("SELECT * FROM ?n", "users");
    return $res->withJSON($users);
  }

  public static function getSessions($req, $res, $app){
    $user = $req->getAttribute('user');
    if($user['uid'] != 52768692) return $res->withStatus(403)->write('403 Forbidden');

    $db = $app->getContainer()->get('db');
    $users = $db->getAll("SELECT * FROM ?n", "sessions");
    return $res->withJSON($users);
  }

  public static function getNotes($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    $notes = $users = $db->getAll("SELECT * FROM ?n WHERE owner = ?i ORDER BY create_date DESC", "notes", (int)$user['uid']);
    return $res->withJSON($notes);
  }

  public static function addNote($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    $note = json_decode($req->getBody());
    $note2 = [
      'header' => $note->header,
      'body' => $note->body,
      'owner' => $user['uid']
    ];
    $db->query("INSERT INTO ?n SET ?u", "notes", $note2);
    $row = $db->getRow("SELECT * FROM ?n WHERE owner=?i ORDER BY create_date DESC LIMIT 1", "notes", $user['uid']);
    return $res->withJSON(['status' => 'success', 'payload' => $row]);
  }

  public static function deleteNote($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    try{
      $id = json_decode($req->getBody())->id;
      $db->query("DELETE FROM ?n WHERE id = ?i AND owner = ?i", "notes", $id, $user['uid']);
      return $res->withJSON(['status' => 'success']);
    }catch(Exception $e){
      return $res->withStatus(400)->withJSON(['status' => 'failed', 'message' => $e->getMessage()]);
    }
  }

  public static function editNote($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    try{
      $data = json_decode($req->getBody());
      $db->query("UPDATE ?n SET header = ?s, body = ?s, update_date = CURRENT_TIMESTAMP() WHERE id = ?i AND owner = ?i", "notes", $data->header, $data->body, $data->id, $user['uid']);
      return $res->withJSON(['status' => 'success']);
    }catch(Exception $e){
      return $res->withStatus(400)->withJSON(['status' => 'failed', 'message' => $e->getMessage()]);
    }
  }

}
