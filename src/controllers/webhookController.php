<?php

require 'autoDeploy.php';

date_default_timezone_set('Europe/Moscow'); // Set this to your local timezone - http://www.php.net/manual/en/timezones.php

class webhookController{
  public static function webhook($req, $res){

    $bitbucket_IP_ranges = array(
    	'131.103.20.160/27',
    	'165.254.145.0/26',
    	'104.192.143.0/24'
    );

    $need_branch = 'master';

    $log_file = '~/deploy.log';

    $path_repo = getcwd();

    // ip с которого пришел запрос
    $ip = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'];

    // Валидация ip запроса
    if (empty($ip) || ! webhookController::check_ip_in_range($ip, $bitbucket_IP_ranges)) {
    	return $res->withStatus(403)->write('403 Forbidden');
    }

    // Проверяем, чтобы ветка была правильной
    $body = json_decode($req->getBody());

    if ( ! $body) {
    	return $res->withStatus(400)->write('400 Bad request');
    }

    $push_branch = $body->push->changes[count($body->push->changes) - 1]->new->name;

    if ($push_branch != $need_branch) {
    	return $res->withStatus(200)->write('Not required branch');
    }

    // Выполняем пулл на сервер
    $depl = new Deploy($path_repo);
    $depl->set_log_file($log_file);

    $depl->execute();
  }

  public static function check_ip_in_range($ip, $cidr_ranges)
  {
  	// Check if given IP is inside a IP range with CIDR format
  	$ip = ip2long($ip);
  	if ( ! is_array($cidr_ranges)) {
  		$cidr_ranges = array($cidr_ranges);
  	}

  	//var_dump($cidr_ranges);

  	foreach ($cidr_ranges as $cidr_range) {
  		list($subnet, $mask) = explode('/', $cidr_range);
  		if (($ip & ~((1 << (32 - $mask)) - 1)) == ip2long($subnet)) {
  			return true;
  		}
  	}

  	return false;
  }

}
