<?php
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\SetCookie;

class authController{

  public static function middleware($req, $res, $next, $app){
    $cors = $app->getContainer()->get('CORS_URL');
    $res = $res->withHeader('Access-Control-Allow-Origin', $cors)->withHeader("Access-Control-Allow-Credentials", "true");

    $cookie = FigRequestCookies::get($req, 'sid');
    $sid = $cookie->getValue();
    if(!$sid) return $res->withStatus(401)->withJSON(['status' => 'failed', 'message' => 'Not Authenticated!']);
    $db = $app->getContainer()->get('db');
    $timeout = $app->getContainer()->get('session_timeout');

    $user = $db->getRow("SELECT u.uid, u.first_name, u.last_name, u.photo_100, u.access_token, s.sid FROM ?n s INNER JOIN ?n u ON u.uid = s.uid WHERE s.sid = ?s and Unix_TIMESTAMP(CURRENT_TIMESTAMP()) - UNIX_TIMESTAMP(s.update_date) < ?i", "sessions", "users", $sid, $timeout);
    if(!$user) return $res->withStatus(401)->withJSON(['status' => 'failed', 'message' => 'Not Authenticated!']);

    $db->query("UPDATE ?n SET update_date = CURRENT_TIMESTAMP() WHERE sid = ?s", "sessions", $user['sid']);

    $req = $req->withAttribute('user', $user);
    return $next($req, $res);
  }

  public static function VkAuth($req, $res, $app){
    $vk = $app->getContainer()->get('vk');
    $authURI = 'https://oauth.vk.com/authorize?response_type=code&display=page&client_id='.
      $vk['id'].
      '&redirect_uri='.$vk['redirect_uri']
    ;

    return $res->withRedirect($authURI);
  }

  public static function logout($req, $res, $app){
    $db = $app->getContainer()->get('db');
    $user = $req->getAttribute('user');
    $db->query("DELETE FROM ?n WHERE sid = ?s", "sessions", $user['sid']);
    return $res->withJSON(['status' => 'success']);
  }

  public static function oauth2callback($req, $res, $app){
    $code = $req->getQueryParam('code');
    $vk = $app->getContainer()->get('vk');
    if($code){
      $tokenURL = "https://oauth.vk.com/access_token?client_id=".
        $vk['id'].
        '&client_secret='.$vk['secret'].
        '&redirect_uri='.$vk['redirect_uri'].
        '&code='.$code
      ;
      if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, $tokenURL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $out = json_decode(curl_exec($curl));
        curl_close($curl);
      }
      if($out && $out->access_token){
        $res = authController::storeUser($out, $app, $res);
      }
    }
    return $res->withRedirect($app->getContainer()->get('afterAuthRedirectUri'));
  }

  public static function getRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public static function storeUser($token, $app, $res){
    $db = $app->getContainer()->get('db');
    $user = authController::getUserInfo($token);
    if($user && $user != 'error'){
      $data = [
        'uid' => $user->uid,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'photo_100' => $user->photo_100,
        'access_token' => $token->access_token
      ];
      $sid = authController::createSession($db, $data);
      $res = FigResponseCookies::set($res, SetCookie::create('sid')
          ->withValue($sid)
          ->rememberForever()
      );
    }
    return $res;
  }

  public static function createSession($db, $data){
    $user = $db->getRow("SELECT * FROM ?n WHERE uid = ?i", "users", $data['uid']);
    if($user){
      $db->query("UPDATE ?n set first_name=?s, last_name=?s, photo_100=?s, access_token=?s, update_date=CURRENT_TIMESTAMP() WHERE uid = ?i",
        "users", $data['first_name'], $data['last_name'], $data['photo_100'], $data['access_token'], $user['uid']
      );
    }else{
      $db->query("INSERT INTO ?n SET ?u", "users", $data);
    }
    $sid = authController::getRandomString(250);
    $session = [
      'sid' => $sid,
      'uid' => $data['uid']
    ];
    $db->query("INSERT INTO ?n SET ?u", "sessions", $session);
    return $sid;
  }

  public static function getUserInfo($token){
    $url = "https://api.vk.com/method/users.get?fields=photo_100&access_token=".$token->access_token.'&user_ids='.$token->user_id;
    if( $curl = curl_init() ) {
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
      $out = json_decode(curl_exec($curl));
      curl_close($curl);
    }
    if($out && $out->response && $out->response[0]){
      return $out->response[0];
    }else if($out && $out->error){
      return 'error';
    }else{
      return null;
    }
  }

}
