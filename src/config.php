<?php

class config{

  public static function getDBinstance(){
    $opts = array(
      'host'      => 'maria',
      'user'      => 'root',
      'pass'      => 'root',
      'db'        => 'simple-notes',
      );
    $db = new SafeMySQL($opts);

    config::createTables($db);

    return $db;
  }

  public static function getSlimOptions(){
    $db = config::getDBinstance();
    $DebugConfig = [
        'settings' => [
            'displayErrorDetails' => true,
        ],
        'db' => $db,
        'session_timeout' => 3600,
        'afterAuthRedirectUri' => 'http://localhost/notes',
        'CORS_URL' => 'http://localhost',
        'vk' => [
          'id' => '6312933',
          'secret' => 'tdYHdBcXJONby59L0n4Z',
          'redirect_uri' => 'https://asksel.ddns.net/oauth2callback'
        ]
    ];

    $ProdConfig = [
        'settings' => [
            'displayErrorDetails' => false,
        ],
        'db' => $db,
        'session_timeout' => 86400,
        'afterAuthRedirectUri' => 'https://simple-notes.ml/notes',
        'CORS_URL' => 'https://simple-notes.ml',
        'vk' => [
          'id' => '6312933',
          'secret' => 'tdYHdBcXJONby59L0n4Z',
          'redirect_uri' => 'https://api.simple-notes.ml/oauth2callback'
        ]
    ];

    //Get env var
    $conf = exec('echo $conf');
    if($conf == 'debug') return $DebugConfig;
    else return $ProdConfig;
  }

  public static function createTables($db){
    $db->query("CREATE TABLE IF NOT EXISTS ?n (
       uid INTEGER NOT NULL UNIQUE,
       first_name varchar(50) NOT NULL,
       last_name varchar(50) NOT NULL,
       photo_100 varchar(250) NOT NULL,
       access_token varchar(250) NOT NULL,
       create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       update_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY(uid)
    ) DEFAULT CHARACTER SET 'utf8';", "users");

    $db->query("CREATE TABLE IF NOT EXISTS ?n (
       sid varchar(250) NOT NULL UNIQUE,
       uid INTEGER NOT NULL,
       create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       update_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY(sid)
    ) DEFAULT CHARACTER SET 'utf8';", "sessions");

    $db->query("CREATE TABLE IF NOT EXISTS ?n (
      id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
      header nvarchar(250) NOT NULL,
      body TEXT NULL,
      owner INTEGER NOT NULL,
      create_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      update_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY(id)
    ) DEFAULT CHARACTER SET 'utf8';", "notes");
  }
}
