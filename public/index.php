<?php
//use \Psr\Http\Message\ServerRequestInterface as Request;
//use \Psr\Http\Message\ResponseInterface as Response;
require '../vendor/autoload.php';

require '../src/controllers/authController.php';
require '../src/controllers/apiController.php';
require '../src/controllers/webhookController.php';
require '../src/config.php';


$rand = authController::getRandomString();

$app = new \Slim\App(config::getSlimOptions());

$app->get('/redirectVK', function($req, $res) use($app) {
  return authController::VkAuth($req, $res, $app);
});

$app->get('/oauth2callback', function($req, $res) use($app) {
  return authController::oauth2callback($req, $res, $app);
});

$app->any('/webhook', function($req, $res){
  return webhookController::webhook($req, $res);
});

$app->group('/private', function () use ($app) {
  $this->get('/logout', function ($req, $res) use ($app) {
      return authController::logout($req, $res, $app);
  });
  $this->get('/getUsers', function ($req, $res) use ($app) {
      return apiController::getUsers($req, $res, $app);
  });
  $this->get('/getUserInfo', function ($req, $res) use ($app) {
      return apiController::getUserInfo($req, $res, $app);
  });
  $this->get('/getSessions', function ($req, $res) use ($app) {
      return apiController::getSessions($req, $res, $app);
  });
  $this->get('/getNotes', function ($req, $res) use ($app) {
      return apiController::getNotes($req, $res, $app);
  });
  $this->post('/addNote', function($req, $res) use($app) {
    return apiController::addNote($req, $res, $app);
  });
  $this->post('/deleteNote', function($req, $res) use($app) {
    return apiController::deleteNote($req, $res, $app);
  });
  $this->post('/editNote', function($req, $res) use($app) {
    return apiController::editNote($req, $res, $app);
  });
})->add(function($req, $res, $next) use ($app){
  return authController::middleware($req, $res, $next, $app);
});

$app->run();
